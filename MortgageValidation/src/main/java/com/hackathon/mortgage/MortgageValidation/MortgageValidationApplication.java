package com.hackathon.mortgage.MortgageValidation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MortgageValidationApplication {

	public static void main(String[] args) {
		SpringApplication.run(MortgageValidationApplication.class, args);
	}

}
